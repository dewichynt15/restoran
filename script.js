const dataMilkshake = [
	{
		nama: 'Milkshake Strawberry',
		harga: 'Rp 25.000',
		gambar: 'foto/ms.webp'
	}, {
		nama: 'Milkshake Bluberry',
		harga: 'Rp 30.000',
		gambar: 'foto/blm.jpg'
	}, {
		nama: 'Milkshake Chocolate',
		harga: 'Rp 25.000',
		gambar: 'foto/cm.jpg'
	}, {
		nama: 'Milkshake Oreo',
		harga: 'Rp 25.000',
		gambar: 'foto/mo.jpg'
	},
];

const dataSmoothies = [
	{
		nama: 'Smoothies Strawberry-Pisang',
		harga: 'Rp 45.000',
		gambar: 'foto/sp.jpg'
	}, {
		nama: 'Smoothies Kiwi-Mangga',
		harga: 'Rp 45.000',
		gambar: 'foto/km.jpg'
	}, {
		nama: 'Smoothies Blueberry-Strawberry',
		harga: 'Rp 55.000',
		gambar: 'foto/sb.jpg'
	}, {
		nama: 'Smoothies Blueberry-Pisang',
		harga: 'Rp 50.000',
		gambar: 'foto/bp.jpg'
	},
]

const dataFrappe = [
	{
		nama: 'Frappucino Oreo',
		harga: 'Rp 35.000',
		gambar: 'foto/oreo.jpg'
	}, {
		nama: 'Frappucino chocolate',
		harga: 'Rp 35.000',
		gambar: 'foto/fc.jpg'
	}, {
		nama: 'Frappucino matcha',
		harga: 'Rp 50.000',
		gambar: 'foto/mf.jpg'
	}, {
		nama: 'Frappucino Strawberry',
		harga: 'Rp 50.000',
		gambar: 'foto/fs.jpg'
	},
]

const containerMilkshake = document.querySelector('.container-milkshake');
const containerSmoothies = document.querySelector('.container-smoothies');
const containerFrappe = document.querySelector('.container-frappe');

for(let data of dataMilkshake){
	containerMilkshake.innerHTML += `
		<div class="card card-promo">
            <div class="animate-img">
                <img src="${data.gambar}" class="card-img-top img-fluid" alt="card1">
            </div>
            <div class="card-body">
                <h5 class="card-title">${data.nama}</h5>
                <p class="card-text">${data.harga}</p>
                <a href="#" class="btn btn-outline-dark">Pesan</a>
            </div>
        </div>
	`;
}

for(let data of dataSmoothies){
	containerSmoothies.innerHTML += `
		<div class="card card-promo">
            <div class="animate-img">
                <img src="${data.gambar}" class="card-img-top img-fluid" alt="card1">
            </div>
            <div class="card-body">
                <h5 class="card-title">${data.nama}</h5>
                <p class="card-text">${data.harga}</p>
                <a href="#" class="btn btn-outline-dark">Pesan</a>
            </div>
        </div>
	`;
}

for(let data of dataFrappe){
	containerFrappe.innerHTML += `
		<div class="card card-promo">
            <div class="animate-img">
                <img src="${data.gambar}" class="card-img-top img-fluid" alt="card1">
            </div>
            <div class="card-body">
                <h5 class="card-title">${data.nama}</h5>
                <p class="card-text">${data.harga}</p>
                <a href="#" class="btn btn-outline-dark">Pesan</a>
            </div>
        </div>
	`;
}